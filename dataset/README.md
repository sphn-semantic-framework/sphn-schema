# SPHN Dataset

This space contains the latest SPHN Dataset release in EXCEL format as well as documentation files, such as new concept documentation and change requests performed. In addition, the templates for proposing new concepts and change requests are made available.

## Documentation of new concepts and changes

New concepts for the SPHN Dataset and all change requests are provided with documentation containing the rationale, reviewed standards, and the new concept content. After the new concept or change is integrated, this documentation will be made available in the respective folder in this space. Further a concept implementation guideline is provided on the [ReadtheDocs](https://sphn-semantic-framework.readthedocs.io) e.g. for Assessments, Lab, Measurement, Omics and Provenance concepts.

## Contact

For any question, please reach out to the Data Coordination Center FAIR Data team at fair-data-team@sib.swiss.
