# Templates

This directory consists of the following:

- [concept_templates](concept_templates): The templates to be used to submit new concepts or changes to the SPHN Dataset
- [dataset_template](dataset_template): The Dataset template for projects to use as a starting point for defining their semantics as a Dataset