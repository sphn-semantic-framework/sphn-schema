# Dataset Template

This directory contains the Dataset template which aims to help projects to
start with the definition of their semantics in the form of concepts (and
composedOfs) following SPHN standards and best practices.

For additional guidance on the use of the SPHN Dataset Template, please read the [SPHN documentation](https://sphn-semantic-framework.readthedocs.io/en/latest/user_guide/project_schema_generation.html).
